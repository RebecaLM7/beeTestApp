package com.hr.beetestapp.data

import com.hr.beetestapp.data.model.LanguageValue
import com.hr.beetestapp.data.model.StoryModel
import java.util.LinkedHashMap

const val NAME_KEY = "name"
const val TITLES_KEY = "titles"
const val IMAGE_URL_KEY = "imageUrl"
const val DESCRIPTIONS_KEY = "descriptions"
const val DYNAMIC_CATEGORY_KEY = "dynamicCategories"
const val LEVEL_V2_KEY = "levelV2"

fun parseToStoryModel(item: LinkedHashMap<String, Any>): StoryModel {
    val storyResult = StoryModel()
    storyResult.name = if (item.containsKey(NAME_KEY)) {
        item[NAME_KEY].toString()
    } else ""

    storyResult.imageUrl = if (item.containsKey(IMAGE_URL_KEY)) {
        item[IMAGE_URL_KEY].toString()
    } else ""

    storyResult.levelV2 = if (item.containsKey(LEVEL_V2_KEY)) {
        item[LEVEL_V2_KEY].toString()
    } else ""

    storyResult.titles = if (item.containsKey(TITLES_KEY)) {
        parseLanguageValue(item[TITLES_KEY])
    } else null

    storyResult.descriptions = if (item.containsKey(DESCRIPTIONS_KEY)) {
        parseLanguageValue(item[DESCRIPTIONS_KEY])
    } else null

    storyResult.dynamicCategories = if (item.containsKey(DYNAMIC_CATEGORY_KEY)) {
        parseLanguageValue(item[DYNAMIC_CATEGORY_KEY])
    } else null

    return storyResult
}

fun parseLanguageValue(valuesList: Any?): ArrayList<LanguageValue> {
    val result = ArrayList<LanguageValue>()
    valuesList?.let { values ->
        if (values is String) {
            if (values.isNotEmpty()) {
                val copy = values.replace("[", "").replace("]", "")
                val list: List<String> = copy.split(",").map { it.trim() }
                list.forEach { item ->
                    if (!item.isNullOrEmpty()) {
                        val valueArray = item.replace("\"", "").split(":")
                        if (valueArray.size == 2) {
                            result.add(LanguageValue(valueArray[0], valueArray[1]))
                        }
                    }
                }
            }
        }
    }

    return result
}