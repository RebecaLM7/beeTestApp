package com.hr.beetestapp.data.model

data class StoryModel(
    var name: String? = null,
    var imageUrl: String? = null,
    var levelV2: String? = null,
    var titles: List<LanguageValue>? = null,
    var descriptions: List<LanguageValue>? = null,
    var dynamicCategories: List<LanguageValue>? = null
)

data class LanguageValue(
    var language: String?,
    var value: String?
)
