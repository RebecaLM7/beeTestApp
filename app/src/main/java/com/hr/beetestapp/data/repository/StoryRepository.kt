package com.hr.beetestapp.data.repository

import com.hr.beetestapp.data.model.StoryModel
import com.hr.beetestapp.data.remote.ERROR_NO_DATA
import com.hr.beetestapp.data.remote.StoryService

class StoryRepository {
    var storyModelList: List<StoryModel> = emptyList()
    var sharedStory: StoryModel = StoryModel()

    suspend fun getStoryListFromApi(): Any {
        val result = StoryService().getStoryList()
        return if (result is List<*> && !result.isNullOrEmpty()) {
            val storyModelFiltered = result.filterIsInstance<StoryModel>()
            if (!storyModelFiltered.isNullOrEmpty()) {
                storyModelList = storyModelFiltered.toList()
                storyModelList
            } else ERROR_NO_DATA
        } else if (result is Int)
            result
        else
            storyModelList
    }
}