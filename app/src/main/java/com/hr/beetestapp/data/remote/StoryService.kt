package com.hr.beetestapp.data.remote

import android.util.Log
import com.hr.beetestapp.data.model.StoryModel
import com.hr.beetestapp.data.parseToStoryModel
import com.kumulos.android.Kumulos
import com.kumulos.android.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

const val ERROR_NO_DATA = 400
const val ERROR_SERVICE = 404

class StoryService {

    suspend fun getStoryList(): Any {
        return withContext(Dispatchers.IO) {
            var resultObject: Any = ERROR_SERVICE
            try {
                val params = HashMap<String, String>()
                Kumulos.call("getAllStoriesV2", params, object : ResponseHandler() {
                    override fun onFailure(error: Throwable?) {
                        super.onFailure(error)
                        resultObject = ERROR_SERVICE
                    }

                    override fun didCompleteWithResult(result: Any?) {
                        super.didCompleteWithResult(result)
                        val storyModelArrayList = ArrayList<StoryModel>()
                        val objects = result as ArrayList<LinkedHashMap<String, Any>>?
                        objects?.let {
                            if (it.isNullOrEmpty()) {
                                resultObject = ERROR_NO_DATA
                                return
                            }
                            for (item in it) {
                                storyModelArrayList.add(parseToStoryModel(item))
                            }
                            resultObject = storyModelArrayList.toList()
                        }
                    }
                })
            } catch (e: Exception) {
                resultObject = ERROR_SERVICE
                e.printStackTrace()
            }
            resultObject
        }
    }
}