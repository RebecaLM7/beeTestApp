package com.hr.beetestapp.ui.view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.hr.beetestapp.data.model.LanguageValue
import com.hr.beetestapp.databinding.FragmentSecondBinding
import com.hr.beetestapp.domain.model.KEY_LANGUAGE_EN
import com.hr.beetestapp.ui.GlideApp
import com.hr.beetestapp.ui.StoryViewModel

class SecondFragment : Fragment() {
    private lateinit var storyViewModel: StoryViewModel
    private lateinit var binding: FragmentSecondBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSecondBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storyViewModel = ViewModelProvider(requireActivity())[StoryViewModel::class.java]
        initObservers()
    }

    private fun initObservers() {
        storyViewModel.storySelected.observe(viewLifecycleOwner) {
            if (!it.imageUrl.isNullOrEmpty()) {
                context?.let { it1 ->
                    GlideApp.with(it1).load(it.imageUrl).into(binding.storyImage)
                }
            }
            val title: LanguageValue? = it.titles?.first { title -> title.language.equals(KEY_LANGUAGE_EN) }
            binding.tvTitle.text = title?.value ?: ""

            binding.tvLevel.text = it.levelV2?.replace("_", " ") ?: ""

            val description: LanguageValue? =
                it.descriptions?.first { description -> description.language.equals(KEY_LANGUAGE_EN) }
            binding.tvDescription.text = description?.value ?: ""
        }
    }
}