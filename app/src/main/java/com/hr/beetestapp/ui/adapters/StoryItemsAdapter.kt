package com.hr.beetestapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hr.beetestapp.data.model.LanguageValue
import com.hr.beetestapp.R
import com.hr.beetestapp.data.model.StoryModel
import com.hr.beetestapp.domain.model.KEY_LANGUAGE_EN
import com.hr.beetestapp.ui.GlideApp
import kotlinx.android.synthetic.main.item_child_layout.view.*

class StoryItemsAdapter(var storyList: List<StoryModel>, var listener: StoryItemListener) :
    RecyclerView.Adapter<StoryItemsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_child_layout, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(storyList[position], listener)
    }

    override fun getItemCount(): Int {
        return storyList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(storyModel: StoryModel, listener: StoryItemListener) {
            itemView.setOnClickListener { listener.onStoryItemListener(storyModel) }
            val title: LanguageValue? = storyModel.titles?.first { it.language.equals(KEY_LANGUAGE_EN) }
            itemView.tv_title.text = title?.value ?: ""
            if (!storyModel.imageUrl.isNullOrEmpty()) {
                GlideApp.with(itemView.context).load(storyModel.imageUrl).into(itemView.story_image)
            }
        }
    }
}

/**
 * Listener of the story item to go to detail page
 * */
interface StoryItemListener {
    fun onStoryItemListener(model: StoryModel)
}