package com.hr.beetestapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hr.beetestapp.R
import com.hr.beetestapp.domain.model.CategoryItemModel
import kotlinx.android.synthetic.main.item_parent_layout.view.*
import java.util.ArrayList

class CategoriesAdapter(var itemListener: StoryItemListener) :
    RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {
    private var categoriesList: List<CategoryItemModel> = ArrayList()

    fun updateCategoriesList(categoriesList: List<CategoryItemModel>) {
        this.categoriesList = categoriesList
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: CategoryItemModel) {
            itemView.category_title.text = item.category
            val storyItemsAdapter = StoryItemsAdapter(item.storyList, itemListener)
            itemView.child_recycler_view.layoutManager =
                LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
            itemView.child_recycler_view.adapter = storyItemsAdapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_parent_layout, parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categoriesList[position])
    }

    override fun getItemCount(): Int = categoriesList.size

}