package com.hr.beetestapp.ui.view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.hr.beetestapp.R
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.hr.beetestapp.data.model.StoryModel
import com.hr.beetestapp.data.remote.ERROR_NO_DATA
import com.hr.beetestapp.databinding.FragmentFirstBinding
import com.hr.beetestapp.ui.StoryViewModel
import com.hr.beetestapp.ui.adapters.CategoriesAdapter
import com.hr.beetestapp.ui.adapters.StoryItemListener

class FirstFragment : Fragment(), StoryItemListener {
    private lateinit var storyViewModel: StoryViewModel
    private lateinit var binding: FragmentFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFirstBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storyViewModel = ViewModelProvider(requireActivity())[StoryViewModel::class.java]
        initUi()
        initObservers()
    }

    private fun initUi() {
        binding.parentRecyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.parentRecyclerView.adapter = CategoriesAdapter(this)
    }

    private fun initObservers() {
        storyViewModel.storyList.observe(viewLifecycleOwner) {
            if (it.isNotEmpty())
                (binding.parentRecyclerView.adapter as CategoriesAdapter).updateCategoriesList(it)
        }
        storyViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }
        storyViewModel.showError.observe(viewLifecycleOwner) {
            val message = when (it) {
                ERROR_NO_DATA -> getString(R.string.error_no_data)
                else -> getString(R.string.error_unknown)
            }
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun navigateToDetailScreen() {
        NavHostFragment.findNavController(this@FirstFragment)
            .navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

    override fun onStoryItemListener(model: StoryModel) {
        storyViewModel.onStorySelected(model)
        navigateToDetailScreen()
    }
}