package com.hr.beetestapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hr.beetestapp.data.model.LanguageValue
import com.hr.beetestapp.data.model.StoryModel
import com.hr.beetestapp.data.remote.ERROR_SERVICE
import com.hr.beetestapp.data.repository.StoryRepository
import com.hr.beetestapp.domain.model.CategoryItemModel
import com.hr.beetestapp.domain.model.mapStoryModelToCategory
import kotlinx.coroutines.launch

class StoryViewModel : ViewModel() {
    val storyList = MutableLiveData<List<CategoryItemModel>>()
    val storySelected = MutableLiveData<StoryModel>()
    var isLoading = MutableLiveData<Boolean>()
    var showError = MutableLiveData<Int>()

    /* Fetch stories data */
    fun fetchSchoolsList() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = StoryRepository().getStoryListFromApi()
            if (result is List<*> && !result.isNullOrEmpty()) {
                val filteredList = mapStoryModelToCategory(result.filterIsInstance<StoryModel>())
                storyList.postValue(filteredList)
            } else if (result is Int) {
                showError.postValue(result)
            } else {
                showError.postValue(ERROR_SERVICE)
            }
            isLoading.postValue(false)
        }
    }

    fun onStorySelected(storyModel: StoryModel) {
        StoryRepository().sharedStory = storyModel
        storySelected.postValue(storyModel)
    }
}