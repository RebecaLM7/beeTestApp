package com.hr.beetestapp.domain.model

import com.hr.beetestapp.data.model.StoryModel

const val KEY_LANGUAGE_EN = "en"

data class CategoryItemModel(val category: String, val storyList: List<StoryModel>)

 fun mapStoryModelToCategory(storyModelList: List<StoryModel>?): List<CategoryItemModel> {
    val categoryList: ArrayList<CategoryItemModel> = ArrayList()
    if (!storyModelList.isNullOrEmpty()) {
        val categoriesList = storyModelList.mapNotNull { storyItem ->
            storyItem.dynamicCategories
        }.flatten().distinct()

        val categoriesEn = categoriesList.filter { it.language.equals(KEY_LANGUAGE_EN, true) }

        categoriesEn.forEach { categoryItem ->
            val filteredList = storyModelList.filter { item ->
                item.dynamicCategories?.contains(categoryItem) ?: false
            }
            categoryList.add(CategoryItemModel(categoryItem.value ?: "", filteredList))
        }
    }
    return categoryList
}