package com.hr.beetestapp.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001c"}, d2 = {"Lcom/hr/beetestapp/ui/adapters/StoryItemsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/hr/beetestapp/ui/adapters/StoryItemsAdapter$ViewHolder;", "storyList", "", "Lcom/hr/beetestapp/data/model/StoryModel;", "listener", "Lcom/hr/beetestapp/ui/adapters/StoryItemListener;", "(Ljava/util/List;Lcom/hr/beetestapp/ui/adapters/StoryItemListener;)V", "getListener", "()Lcom/hr/beetestapp/ui/adapters/StoryItemListener;", "setListener", "(Lcom/hr/beetestapp/ui/adapters/StoryItemListener;)V", "getStoryList", "()Ljava/util/List;", "setStoryList", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_debug"})
public final class StoryItemsAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.hr.beetestapp.ui.adapters.StoryItemsAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.hr.beetestapp.data.model.StoryModel> storyList;
    @org.jetbrains.annotations.NotNull()
    private com.hr.beetestapp.ui.adapters.StoryItemListener listener;
    
    public StoryItemsAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.hr.beetestapp.data.model.StoryModel> storyList, @org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.StoryItemListener listener) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.hr.beetestapp.data.model.StoryModel> getStoryList() {
        return null;
    }
    
    public final void setStoryList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.hr.beetestapp.data.model.StoryModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.hr.beetestapp.ui.adapters.StoryItemListener getListener() {
        return null;
    }
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.StoryItemListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.hr.beetestapp.ui.adapters.StoryItemsAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.StoryItemsAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"Lcom/hr/beetestapp/ui/adapters/StoryItemsAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "storyModel", "Lcom/hr/beetestapp/data/model/StoryModel;", "listener", "Lcom/hr/beetestapp/ui/adapters/StoryItemListener;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.hr.beetestapp.data.model.StoryModel storyModel, @org.jetbrains.annotations.NotNull()
        com.hr.beetestapp.ui.adapters.StoryItemListener listener) {
        }
    }
}