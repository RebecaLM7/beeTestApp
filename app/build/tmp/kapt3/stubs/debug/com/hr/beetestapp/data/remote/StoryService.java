package com.hr.beetestapp.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u0001H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0005"}, d2 = {"Lcom/hr/beetestapp/data/remote/StoryService;", "", "()V", "getStoryList", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class StoryService {
    
    public StoryService() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getStoryList(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<java.lang.Object> continuation) {
        return null;
    }
}