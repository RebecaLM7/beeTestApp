package com.hr.beetestapp.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u000f\u001a\u00020\u0001H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\nR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lcom/hr/beetestapp/data/repository/StoryRepository;", "", "()V", "sharedStory", "Lcom/hr/beetestapp/data/model/StoryModel;", "getSharedStory", "()Lcom/hr/beetestapp/data/model/StoryModel;", "setSharedStory", "(Lcom/hr/beetestapp/data/model/StoryModel;)V", "storyModelList", "", "getStoryModelList", "()Ljava/util/List;", "setStoryModelList", "(Ljava/util/List;)V", "getStoryListFromApi", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getStoryListFromLocal", "app_debug"})
public final class StoryRepository {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.hr.beetestapp.data.model.StoryModel> storyModelList;
    @org.jetbrains.annotations.NotNull()
    private com.hr.beetestapp.data.model.StoryModel sharedStory;
    
    public StoryRepository() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.hr.beetestapp.data.model.StoryModel> getStoryModelList() {
        return null;
    }
    
    public final void setStoryModelList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.hr.beetestapp.data.model.StoryModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.hr.beetestapp.data.model.StoryModel getSharedStory() {
        return null;
    }
    
    public final void setSharedStory(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.data.model.StoryModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getStoryListFromApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<java.lang.Object> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.hr.beetestapp.data.model.StoryModel> getStoryListFromLocal() {
        return null;
    }
}