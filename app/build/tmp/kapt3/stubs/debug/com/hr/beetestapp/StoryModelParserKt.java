package com.hr.beetestapp;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000*\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a \u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u001a\u001a\u0010\r\u001a\u00020\u000e2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\f0\u0010\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"DESCRIPTIONS_KEY", "", "DYNAMIC_CATEGORY_KEY", "IMAGE_URL_KEY", "LEVEL_V2_KEY", "NAME_KEY", "TITLES_KEY", "parseLanguageValue", "Ljava/util/ArrayList;", "Lcom/hr/beetestapp/data/model/LanguageValue;", "Lkotlin/collections/ArrayList;", "valuesList", "", "parseToStoryModel", "Lcom/hr/beetestapp/data/model/StoryModel;", "item", "Ljava/util/LinkedHashMap;", "app_debug"})
public final class StoryModelParserKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NAME_KEY = "name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TITLES_KEY = "titles";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMAGE_URL_KEY = "imageUrl";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DESCRIPTIONS_KEY = "descriptions";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DYNAMIC_CATEGORY_KEY = "dynamicCategories";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LEVEL_V2_KEY = "levelV2";
    
    @org.jetbrains.annotations.NotNull()
    public static final com.hr.beetestapp.data.model.StoryModel parseToStoryModel(@org.jetbrains.annotations.NotNull()
    java.util.LinkedHashMap<java.lang.String, java.lang.Object> item) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.ArrayList<com.hr.beetestapp.data.model.LanguageValue> parseLanguageValue(@org.jetbrains.annotations.Nullable()
    java.lang.Object valuesList) {
        return null;
    }
}