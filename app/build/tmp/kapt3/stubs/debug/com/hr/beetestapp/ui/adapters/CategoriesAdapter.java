package com.hr.beetestapp.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0017B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\f\u001a\u00020\rH\u0016J\u001c\u0010\u000e\u001a\u00020\u000f2\n\u0010\u0010\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0011\u001a\u00020\rH\u0016J\u001c\u0010\u0012\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016J\u0014\u0010\u0016\u001a\u00020\u000f2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\u0005\u00a8\u0006\u0018"}, d2 = {"Lcom/hr/beetestapp/ui/adapters/CategoriesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/hr/beetestapp/ui/adapters/CategoriesAdapter$ViewHolder;", "itemListener", "Lcom/hr/beetestapp/ui/adapters/StoryItemListener;", "(Lcom/hr/beetestapp/ui/adapters/StoryItemListener;)V", "categoriesList", "", "Lcom/hr/beetestapp/domain/model/CategoryItemModel;", "getItemListener", "()Lcom/hr/beetestapp/ui/adapters/StoryItemListener;", "setItemListener", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "updateCategoriesList", "ViewHolder", "app_debug"})
public final class CategoriesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.hr.beetestapp.ui.adapters.CategoriesAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private com.hr.beetestapp.ui.adapters.StoryItemListener itemListener;
    private java.util.List<com.hr.beetestapp.domain.model.CategoryItemModel> categoriesList;
    
    public CategoriesAdapter(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.StoryItemListener itemListener) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.hr.beetestapp.ui.adapters.StoryItemListener getItemListener() {
        return null;
    }
    
    public final void setItemListener(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.StoryItemListener p0) {
    }
    
    public final void updateCategoriesList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.hr.beetestapp.domain.model.CategoryItemModel> categoriesList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.hr.beetestapp.ui.adapters.CategoriesAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.hr.beetestapp.ui.adapters.CategoriesAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/hr/beetestapp/ui/adapters/CategoriesAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Lcom/hr/beetestapp/ui/adapters/CategoriesAdapter;Landroid/view/View;)V", "bind", "", "item", "Lcom/hr/beetestapp/domain/model/CategoryItemModel;", "app_debug"})
    public final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.hr.beetestapp.domain.model.CategoryItemModel item) {
        }
    }
}