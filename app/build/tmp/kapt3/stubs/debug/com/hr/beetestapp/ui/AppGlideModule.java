package com.hr.beetestapp.ui;

import java.lang.System;

@com.bumptech.glide.annotation.GlideModule()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/hr/beetestapp/ui/AppGlideModule;", "Lcom/bumptech/glide/module/AppGlideModule;", "()V", "app_debug"})
public final class AppGlideModule extends com.bumptech.glide.module.AppGlideModule {
    
    public AppGlideModule() {
        super();
    }
}